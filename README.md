# Changer
A script tha changes the wallpaer of a gnome desktop periodicaly
# How to use
- Clone the repository in ~/.config/
- Change the variables in `changer.conf`
  - WALLPAPER_CHANGE_PERIOD: Time between wallpaper changes
  - WALLPAPERS: Directory from where to take the wallpapers
- Copy `wallpaper_changer.desktop` to ~/.config/autostart