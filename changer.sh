#!/bin/bash

{
	while [ true ] 
	do
		sleep $WALLPAPER_CHANGE_PERIOD
		source $HOME/.config/wallpaper/changer.conf
		gsettings set org.gnome.desktop.background picture-uri $WALLPAPERS$(ls $WALLPAPERS |sort -R|tail -n1)
	done

} &
